import axios from "axios";

const axiosQuote = axios.create({
  baseURL: "https://alimbekov-exam-8-default-rtdb.firebaseio.com/",
});

export default axiosQuote;
